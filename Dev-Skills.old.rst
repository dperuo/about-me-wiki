Skills List
===========

Hello and Welcome to my Dev Skills List. It details all the skills and technologies I've used on projects. It's a living document that updates often.

.. contents::
    :depth: 2
    :backlinks: none

Top Skills
----------

- Angular + Ionic
- HTML + CSS
- Laravel + PHP
- Python + Bash



Client-Side & Design
--------------------

Client-Side Scripting
^^^^^^^^^^^^^^^^^^^^^

- AJAX
- Angular
- Asynchronous Functions
- Callbacks
- CoffeeScript
- Conditionizr.js
- Cookies
- CSON
- IndexedDB
- Ionic
- JavaScript
- jQuery
- JSON
- Knockout.js
- LocalStorage
- Marionette.js
- Modernizr.js
- Moment.js
- Observables
- Promises
- Query Strings
- Redux
- Regex
- Regular Expressions
- Require.js
- RESTful API
- Service Workers
- SessionStorage
- TypeScript
- URL Query Strings
- Web Workers
- YAML
- Zone.js


CSS & Design
^^^^^^^^^^^^

- Adobe Illustrator
- Adobe InDesign
- Adobe Photoshop
- Atomic Design
- BEM Syntax
- Bootstrap CSS
- Compass
- Foundation CSS
- HTML5 Boilerplate
- Inuit CSS
- Ionic
- Less CSS
- Pure CSS
- Sass
- Scss
- SMACCS
- Stylus
- UI Bootstrap


HTML Templates
^^^^^^^^^^^^^^

- Blade
- Fountain
- Handlebars
- Jade
- Jekyll
- Liquid
- Markdown
- Mustache
- Smarty
- Twig


Content Management
^^^^^^^^^^^^^^^^^^

- Shopify
- Squarespace
- Squareup
- Tumblr
- Wordpress



Server-Side & Dev-Ops
---------------------

Server-Side Scripting
^^^^^^^^^^^^^^^^^^^^^

- Bash Shell
- Laravel
- PHP
- Postman
- Python
- Regex
- Regular Expressions
- Shell Scripting
- Vimscript
- Zsh Shell


Automation & Deployment
^^^^^^^^^^^^^^^^^^^^^^^

- Akamai
- Bower
- Browserify
- CodeShip
- Elixir
- FTP
- Grunt
- Gulp
- Homebrew
- Makefile
- npm
- RubyGems
- SFTP
- SSH
- SystemJS
- Tmux
- Travis CI
- Webpack
- Yarn
- Yeoman


Databases & Platforms
^^^^^^^^^^^^^^^^^^^^^

- Amazon Web Service
- Anvil
- Apache
- AWS
- CentOS Linux
- CouchDB
- Debian Linux
- Docker
- Docker Compose
- Fedoria Linux
- iOS
- Kali Linux
- MacOS
- Mint Linux
- MySQL
- Nginx
- Oracle
- OS X
- PouchDB
- Redis
- SAP HANA
- Ubuntu Linux
- Vagrant
- WebAPI



Collaboration & Documentation
-----------------------------

- ADR
- Agile
- Architecture Decision Records
- Asana
- AsciiDoc
- BDD
- Behavior Driven Design
- Bitbucket
- Dropbox
- EditorConfig
- ESLint
- Feature Flags
- Feature Toggles
- Git
- GitHub Flow
- Google Drive
- HipChat
- Jira
- JSCS
- JSDoc
- JSHint
- Markdown
- Mercurial
- Postman
- reStructuredText
- Scrum
- Slack
- SourceTree
- Swagger
- TDD
- TDR
- Technical Debt Records
- Test Driven Design
- Toggl
- Trello
- TSLint
- Zoom


Testing & Optimization
----------------------

- Behat
- Chrome DevTools
- Firebug
- Gherkin
- Google PageSpeed
- Jasmine
- Karma
- Phantom.js
- PHPUnit
- Protractor
- Selenium Webdriver
- WebPageTest.org



Experimenting With
------------------

- Ansible
- Vault (HashiCorp)
