Hello and welcome! Please click the links to learn more about me and how I like to work.

[[User Manual]]
  Who I am, what I value, and how best to work with me.

[[Playbook]]
  How I like to build projects.

[[Dev Skills]]
  Technologies I know and use.
