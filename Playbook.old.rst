.. _thoughtbot: http://playbook.thoughtbot.com/
.. _Hanno: http://playbook.hanno.co/

Hello and welcome to my playbook. It details how I to build projects. It's a living document that updates often. Hat tips to `thoughtbot`_ and `Hanno`_ for the idea.

**Last Update: 2018-05-31**

.. contents::
    :depth: 2
    :backlinks: none


Use Systems and Checklists
==========================

Use `good defaults <https://en.wikipedia.org/wiki/Convention_over_configuration>`_. Use systems and checklists. Use common architectures and style guides for all projects.


Keep Things Simple and Easy to Understand
=========================================

Look for simple, elegant solutions. Look for future-proof solutions. Subtract and delete before adding new code. The best solutions `look like magic <https://www.youtube.com/watch?v=r2CbbBLVaPk>`_ and should `Just Work™️ <https://en.wikipedia.org/wiki/Principle_of_least_astonishment>`_.



Done Is Better Than Perfect
===========================

Perfection takes too long. Iterate `often <https://www.youtube.com/watch?v=jHyU54GhfGs>`_. Keep the `feedback loop <https://en.wikipedia.org/wiki/OODA_loop>`_ as short as possible. `Disagree and commit <https://www.amazon.jobs/principles>`_ when needed.



Work Autonomously, Take Ownership
=================================

No blame. No excuses. Say what you mean, mean what you say, don't be mean when you say it. Deliver as promised. Trust your team to do the same.



Ask Questions, Give Answers
===========================

Communicate with your team. Ask questions, share answers. Avoid silos. Spread knowledge fast. Trust your team to do the same.



Don't Repeat Yourself
=====================

Don't reinvent the wheel. Start with existing options and iterate from there.



Automate Everything
===================

Automate everything. This lets you focus on `deep work <http://calnewport.com/blog/2012/11/21/knowledge-workers-are-bad-at-working-and-heres-what-to-do-about-it/>`_.



Code Should Do One Thing Well
=============================

Code should follow `Unix philosophy <https://en.wikipedia.org/wiki/Unix_philosophy>`_:

#. Each thing does one thing well.
#. Each thing is self-contained.
#. Larger, more complex things build on top of smaller, simpler things.
#. Things can work together using a simple API.


Code Should Be Explicit
=======================

The purpose of your code should be clear, direct, and easy to grok without searching the rest of the codebase. Avoid implied code. Leave no room for confusion or doubt about what your code does.



Use the Best Tool for the Job
=============================

Don't put `square pegs in round holes <https://en.wikipedia.org/wiki/Square_peg_in_a_round_hole>`_. Use the right tool for the task at hand.



Keep the End User in Mind
=========================

Users should be the heroes of their own stories. Actions should have a narrative with well-defined beginnings, middles, and ends. `Don't make users think <http://www.uxbooth.com/articles/10-usability-lessons-from-steve-krugs-dont-make-me-think/>`_. Reward users when they succeed. The best narratives `adapt to the user <http://www.uxbooth.com/articles/progressive-content/>`_.
