I assess a single fee for my consulting work based on my contribution to the value you’ll be receiving.

There is never a "meter running" on our relationship and you do not have to make an investment decision every time my assistance is needed. I will commit as much time as necessary to fulfill the objectives and time frames we establish, and your people never have to seek permission to spend money if they need my help. This is a unique feature of my consulting practice.

While most clients enjoy my single fee, some clients insist on time-based billing. I charge a retainer for time-based billing and all work is billed against the retainer. The retainer fee is based on my contribution to the value you’ll receive.
