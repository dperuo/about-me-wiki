.. _Leah Fessler: https://qz.com/1046131/writing-a-user-manual-at-work-makes-teams-less-anxious-and-more-productive/
.. _Abby Falik: https://www.linkedin.com/pulse/leaders-need-user-manuals-what-i-learned-writing-mine-abby-falik

Hello and welcome to my user manual. It details who I am, what I value, and how best to work with me. Hat tips for the idea go to `Leah Fessler`_ and `Abby Falik`_.

**Last Update: 2018-05-31**

.. contents::
    :depth: 2
    :backlinks: none


Core Values
===========

Do Good Work
  Create “Good Karma” in the universe. You should always feel comfortable telling your mother about what you do and how you do it.

Deliver as Promised
  Say what you mean, mean what you say, don't be mean when you say it. Trust your team to do the same.

Pay It Forward
  Empower others to reach their goals. Spread knowledge fast. Teach others what you know. Learn new things.

Keep It Fun
  Act with kindness and love. Find the joy. Leave people with happy memories.

Take Ownership
  No blame. No excuses. How you do anything is how you do everything.

Play to Win
  *Winning* is very different than *not losing*. If we're going to play the game, play the game to win. Crush it, every day.



My Style
========

Maximize ROI
  Focus on value. Leverage your time and money. Use force multipliers. `Do less <https://www.youtube.com/watch?v=TrvLEgPpV8s>`_. Find the `Objectives, Key Results <https://en.wikipedia.org/wiki/OKR>`_, and `Key Performance Indicator <https://www.klipfolio.com/resources/kpi-examples>`_.

Predict and Delegate
  *"The two most important attributes of effective leaders are their ability to predict and to delegate."* ~ `Verne Harnish <https://www.amazon.com/Mastering-Rockefeller-Habits-Increase-Growing/dp/0978774957>`_

Done Is Better Than Perfect
  Perfection takes too long. Iterate `often <https://www.youtube.com/watch?v=jHyU54GhfGs>`_. Keep the `feedback loop <https://en.wikipedia.org/wiki/OODA_loop>`_ as short as possible. `Disagree and commit <https://www.amazon.jobs/principles>`_ when needed.

Work in Small Blocks of Time
  I break my day into small blocks of time like `Bill Gates and Elon Musk <http://www.businessinsider.com/bill-gates-elon-musk-scheduling-habit-2017-8>`_. My blocks are 6 minutes long. I fit all my work into multiples of `these blocks <https://gist.github.com/dperuo/f29a48fce8d306140a46e3bbed422ea0>`_.

Schedule Meetings in Advance
  Scheduled all meetings with me in advance. Shorter meetings are better than longer meetings.

Use the Playbook
  When in doubt, consult the [[Playbook]].



How to Communicate With Me
==========================

Be Blunt
  Tell me what I need to know, not what you think I want to hear. Never  `sugarcoat <https://en.wiktionary.org/wiki/sugarcoat>`_ or  `bury the lead <https://en.wiktionary.org/wiki/bury_the_lead#English>`_. I cannot read your mind or second-guess what you're trying to say.

Start at the End
  Put the  `bottom line up front <https://hbr.org/2016/11/how-to-write-email-with-military-precision>`_. Give me the  `TL;DR <https://en.wikipedia.org/wiki/TL;DR>`_. I'll probe for details as I need them.

No Blame, No Excuses
  Say what you mean, mean what you say, don't be mean when you say it. I will do the same.

Offer Solutions
  Always come to me with good solutions and your recommended plan of action.

Disagree When Needed
  If I say something you disagree with, tell me. I respect people who have the  `confidence <https://www.amazon.jobs/principles>`_ to question their colleagues.

Protect Your Time
  Time is a resource. Know your limits. Don't take on more than you can do in the time you have. Tell me when you are at capacity and when your plate is full.



How to Help Me
==============

Cover the Details
  I move fast and don’t always catch every detail. Please cover the details, and flag me down for details that need my attention.

Raise Issues Early
  If there's a mistake or something is heading off the rails, please tell me before the crash. Failure is great; surprises are not.

Give Me a Reality Check
  I set the bar very high. Please tell me if I'm acting like a dick or being unrealistic about what we can do in the time we have.

Find a Better Way
  I like new ideas and better options. I  `act fast <https://digitalkickstart.com/the-4070-rule-and-how-it-applies-to-you/>`_ and will pivot as needed to move in a different direction.



Work Hours
==========

I Don't Track the Hours You Work
  My focus is on the value we bring to our clients and customers. If you can do that in 30 minutes while sitting on a park bench in Zurich, then do it!

Make Team Communication Easy
  We are a global team and work across a lot of time zones. Please make communication easy. Keep your workday within standard working hours: 7am – 7pm in your time zone. Be clear with your team that you are working between X and Y times and coordinate as needed.

Shift Your Workday as Needed
  You can shift your workday and take time off as needed. Make sure there is no material impact on the needs of your teams, your projects, or the quality of your results.

Don't Work When You're Tired
  I expect high-quality work at all times and this won’t happen at 4 am on five hours of sleep. Talk to your manager to get your workload re-assessed or take time off if you have something going on.

Don't Work on Vacation
  If you are on vacation you should not be working. Take care of yourself and your family. Get some sleep. Eat well. Visit nature and unplug. Work hard when it’s time to work and enjoy the time off when you have time off. This is both healthy for you and keeps scheduling clear and clean with the team.
