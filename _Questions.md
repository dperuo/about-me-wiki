1. What appeals to you most about working with me?

1. How important is this project compared to other initiatives?

1. Have you made a commitment to proceed, or are you still analyzing?

1. Who controls the resources required to make this happen?

1. Have you tried this before? What were the results?

1. Imagine the ideal outcome for this project. What does that look like?

1. How does the ideal outcome make you feel?

1. What will these results mean for your organization?

1. How would you feel if this project fails?

1. Are you awaiting the results of any other initiatives or decisions?

1. Do you have to seek anyone else’s approval?

1. In the past, what has occurred to derail potential projects like this?

1. Is anything likely to change in the organization in the near future?

1. What haven’t I asked you that I should have about the environment?

1. Is there anything we haven’t discussed which could get in the way?

1. What, if anything, do you additionally need to hear from me?

1. If I get this proposal to you tomorrow, how soon will you decide?
